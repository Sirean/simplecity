﻿using UnityEngine;

public class CameraDrag : MonoBehaviour
{
    private Vector3 _dragOrigin;
    public float DragSpeed = 2;

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _dragOrigin = Input.mousePosition;
            return;
        }

        if (!Input.GetMouseButton(1)) return;

        var pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _dragOrigin);
        var move = new Vector3(pos.x*DragSpeed, 0, pos.y*DragSpeed);

        transform.Translate(move, Space.World);
    }
}