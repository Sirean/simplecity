﻿using UnityEngine;

public class TileMouseOver : MonoBehaviour
{
    private Color _normalColor;
    public Color HighlightColor;

    private void Start()
    {
        _normalColor = GetComponent<Renderer>().material.color = Color.white;
    }

    private void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (GetComponent<Collider>().Raycast(ray, out hitInfo, Mathf.Infinity))
        {
            GetComponent<Renderer>().material.color = HighlightColor;
        }  
        else
        {
            GetComponent<Renderer>().material.color = _normalColor;
        }
    }
}