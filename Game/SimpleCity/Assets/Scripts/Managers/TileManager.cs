﻿using UnityEngine;

public  class TileManager : Manager
{
    public GameObject[,] _gamegrid = new GameObject[10, 10];
    public int Height = 10;
    public GameObject Plane;
    private GameObject _parent; //aanpassen aan iemands style erg belangerijk
    public int Width = 10;

    /// <summary>
    /// This instance overrides the managers instance
    /// </summary>
    public override  void Initialize()
    {
        _parent = GameObject.Find("TileParent");
        for (var x = 0; x < Width; x ++)
        {
            for (var z = 0; z < Height; z++)
            {
                var gridPlane = MonoBehaviour.Instantiate(Plane);
                gridPlane.transform.position = new Vector3(gridPlane.transform.position.x + x,
                    gridPlane.transform.position.y, gridPlane.transform.position.z + z);
                _gamegrid[x, z] = gridPlane;
               gridPlane.transform.parent = _parent.transform;
            }


        }
    }
}